# NixOS(-compose) image with Numpex tools

This repo presents a NixOS configuration and a NixOS-compose composition with Guix, Nix, and Spack available.

## NixOS

### Virtual Machine

Build:

```
nixos-rebuild build-vm --flake .#numpex
```

Start:

```
./result/bin/run-nixos-vm
```

### Container

(need to be root)

Build:

```
sudo nixos-container create numpex --flake .
```

Start:

```
sudo nixos-container start numpex
```

Connect:

```
sudo nixos-container root-login numpex
```

Stop:

```
sudo nixos-container stop numpex
```

## NixOS-compose

Enter the shell:

```
nix develop
```

Build:

```
nxc build -f <FLAVOUR>
```

Start:

```
nxc start
nxc connect numpex
```

## Misc

Was tested with g5k-image, vm, docker
