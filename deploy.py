from nixos_compose.nxc_execo import get_oar_job_nodes_nxc

from execo import Process, SshProcess, Remote
from execo_g5k import oardel, oarsub, OarSubmission, get_oar_job_nodes
from execo_engine import Engine, logger, ParamSweeper, sweep

import sys
import os


class MyEngine(Engine):
    def __init__(self):
        super(MyEngine, self).__init__()
        parser = self.args_parser
        parser.add_argument('--nxc_build_file', help='Path to the NXC deploy file')
        self.nodes = {}
        self.oar_job_id = -1

    def init(self):
        nb_nodes = 1
        site = "grenoble"
        cluster = "dahu"

        nxc_build_file = self.args.nxc_build_file

        print(nxc_build_file)
        oar_job = reserve_nodes(nb_nodes, site, cluster, walltime=15*60)
        self.oar_job_id, site = oar_job[0]
        roles_quantities = {"numpex": ["numpex"]}
        self.nodes = get_oar_job_nodes_nxc(self.oar_job_id, site, flavour_name="g5k-image", compose_info_file=nxc_build_file, roles_quantities=roles_quantities)
        print(self.nodes)

    def run(self):
        my_command = "echo \"Hello from $(whoami) at $(hostname) ($(ip -4 addr | grep \"/20\" | awk '{print $2;}'))\" > /tmp/hello"
        hello_remote = Remote(my_command, self.nodes["numpex"], connection_params={'user': 'root'})
        hello_remote.run()

        my_command2 = "cat /tmp/hello"
        cat_remote = Remote(my_command2, self.nodes["numpex"], connection_params={'user': 'root'})
        cat_remote.run()
        for process in cat_remote.processes:
            print(process.stdout)

def reserve_nodes(nb_nodes, site, cluster, walltime=3600):
    jobs = oarsub([(OarSubmission("{{cluster='{}'}}/nodes={}".format(cluster, nb_nodes), walltime, job_type=["allow_classic_ssh", "deploy"]), site)])
    return jobs

if __name__ == "__main__":
    ENGINE = MyEngine()
    try:
        ENGINE.start()
    except Exception as ex:
        print(f"Failing with error {ex}")
        oardel([(ENGINE.oar_job_id, None)])
        print("Giving back the resources")

