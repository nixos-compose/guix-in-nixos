{
  description = "NixOS image for Numpex tools";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/23.05";
    nxc.url = "git+https://gitlab.inria.fr/nixos-compose/nixos-compose.git";
    nuix.url = "github:GuilloteauQ/nix-overlay-guix";
  };

  outputs = { self, nixpkgs, nxc, nuix }:
    let system = "x86_64-linux";
    in {
      nixosConfigurations = let
        imageConfig = isContainer:
          nixpkgs.lib.nixosSystem {
            inherit system;
            modules = [
              ({ pkgs, ... }:
                let
                  spackStoreLocation = "/tmp/spack";
                  spack = pkgs.callPackage ./spack.nix {
                    inherit spackStoreLocation;
                  };
                in {
                  boot.isContainer = isContainer;
                  nixpkgs.overlays = [ nuix.overlays.default ];
                  imports = [ nuix.nixosModules.guix ];
                  services.guix.enable = true;
                  environment.systemPackages = [ spack ];
                  users.extraUsers.root.password = "root";
                })
            ];
          };

      in {
        numpex = imageConfig false;
        container = imageConfig true;
      };

      packages.${system} = nxc.lib.compose {
        inherit nixpkgs system;
        overlays = [ nuix.overlays.default ];
        extraConfigurations = [ nuix.nixosModules.guix ];
        composition = ./composition.nix;
      };

      defaultPackage.${system} = self.packages.${system}."composition::vm";

      devShell.${system} = nxc.devShells.${system}.nxcShellFull;
    };
}
