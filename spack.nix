{ spackStoreLocation ? "/tmp/spack", python3, python3Packages, clingo, gnumake
, writeShellApplication, writeTextDir, stdenv, fetchgit, fetchFromGitHub, cmake
, gcc }:

let
  spackConfig = writeTextDir "config.yaml" ''
    config:
      install_tree:
        root: ${spackStoreLocation}/opt/spack
        projections:
          all: "{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash}"
      template_dirs:
        - ${spackStoreLocation}/share/spack/templates
      license_dir: ${spackStoreLocation}/etc/spack/licenses
      build_stage:
        - $tempdir/$user/spack-stage
        - $user_cache_path/stage
      test_stage: $user_cache_path/test
      source_cache: ${spackStoreLocation}/var/spack/cache
      misc_cache: $user_cache_path/cache
      connect_timeout: 10
      verify_ssl: true
      suppress_gpg_warnings: false
      install_missing_compilers: false
      checksum: true
      deprecated: false
      dirty: false
      build_language: C
      locks: true
      url_fetch_method: urllib
      ccache: false
      concretizer: clingo
      db_lock_timeout: 60
      package_lock_timeout: null
      shared_linking:
        type: rpath
        bind: false
      allow_sgid: true
      install_status: true
      binary_index_ttl: 600
      flags:
        keep_werror: 'none'
            '';
  clingo-ffi = stdenv.mkDerivation {
    name = "clingo";
    version = "master";
    src = fetchgit {
      url = "https://github.com/potassco/clingo";
      rev = "ec2c706656d7e8751b666aa31199c598c6487af6";
      sha256 = "sha256-3rEBfpivTSs/MxBp1MVncNR8Z1cptt2ndyT+99tphMw=";
      fetchSubmodules = true;
    };
    nativeBuildInputs = [ cmake ];
    buildInputs = [ clingo ];
    propagatedBuildInputs = [ python3Packages.scikit-build ];
  };
  spack = python3Packages.buildPythonPackage rec {
    name = "spack";
    version = "0.21.0";
    src = fetchFromGitHub {
      owner = "spack";
      repo = "spack";
      rev = "v0.20.1";
      sha256 = "sha256-cXMvdhl46LkswEVzwN8pVhPQjrnPcO7W2GQzsPr81kI=";
    };
    format = "pyproject";
    propagatedBuildInputs = [ python3Packages.hatchling clingo clingo-ffi ];
    doCheck = false;
  };
in writeShellApplication {
  name = "spack";
  runtimeInputs =
    [ spack python3 python3Packages.hatchling clingo clingo-ffi gnumake gcc ];
  text = ''
    spack -C ${spackConfig} "$@"
  '';
}
